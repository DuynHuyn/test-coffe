"use strict";

let myRequest = new Request("./js/data.json");
let arr = [];

fetch(myRequest)
    .then(function(resp) {
        // console.log(resp);
        return resp.json();
    })
    .then(function(data) {
        // console.log(data);
        // const x = JSON.parse(data);
        // console.log(x);
        document.getElementById("menu").innerHTML = `${data.map(function(item) {
        return `
        <a class="category-item" href="#" onclick="show(event, '${item.slug}')">
            <img src="${item.thumbnail}" alt="ca phe">
            <p>${item.name}</p>
        </a>
        `
        }).join('')}`;


        data.map(function(item){
            item.products.forEach(x => {
                const p = {...x, slug: item.slug};
                arr.push(p);
            });
        })

        // console.log(arr);

        document.getElementById("product").innerHTML = `${arr.map(function(p) {
            return `<div class="col c-6 m-4 l-3 ${p.slug}">
                        <div class="product-item">
                            <div class="product-img">
                                <img src="${p.thumbnail}" alt="product-img">
                            </div>
                            <div class="product-des">
                                <p class="product-name">${p.name}</p>
                                <p class="product-price">${p.base_price_str}</p>
                            </div>
                        </div>
                    </div>`
        }).join('')}`

        // document.getElementById("product").innerHTML = `${data.map(function(item) {
        //         // console.log(item.products);

        //         item.products.map(function(p) {
        //             // console.log(p.name)
        //             // console.log(item.slug)
        //             return `<div class="col c-3 ${item.slug}">
        //                 <div class="product-item">
        //                     <div class="product-img">
        //                         <img src="${p.thumbnail}" alt="product-img">
        //                     </div>
        //                     <div class="product-des">
        //                         <p class="product-name">${p.name}</p>
        //                         <p class="product-price">${p.base_price_str}</p>
        //                     </div>
        //                 </div>
        //             </div>`

        //             // return `<p>hjdfj</p>`
        //         })
        //     }).join('')
        // }`

        
    });
